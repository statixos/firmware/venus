# vendor_xiaomi_venus-firmware

Firmware images for Mi 11 (venus), to include in custom ROM builds.

**Current version**: fw_venus_miui_VENUSGlobal_V14.0.4.0.TKBMIXM_6040ecb479_13.0

### How to use?

1. Clone this repo to `vendor/xiaomi/venus-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/venus-firmware/BoardConfigVendor.mk
```

